﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Back_api.Models;

namespace Back_api.Data
{
    public partial class DbAPiContext : DbContext
    {
        public DbAPiContext()
        {
        }

        public DbAPiContext(DbContextOptions<DbAPiContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Instrumento> Instrumentos { get; set; } = null!;
        public virtual DbSet<Inventario> Inventarios { get; set; } = null!;
        public virtual DbSet<Marca> Marcas { get; set; } = null!;
        public virtual DbSet<VwInfo> VwInfos { get; set; } = null!;

//         protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//         {
//             if (!optionsBuilder.IsConfigured)
//             {
// #warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//                 optionsBuilder.UseSqlServer("Data Source=192.168.3.131;Database=DbAPi;Trusted_Connection=Yes");
//             }
//         }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Instrumento>(entity =>
            {
                entity.Property(e => e.FechaCreacion).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.IdMarca).HasDefaultValueSql("((1))");
            });

            modelBuilder.Entity<VwInfo>(entity =>
            {
                entity.ToView("vw_info");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
