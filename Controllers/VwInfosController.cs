#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Back_api.Data;
using Back_api.Models;

namespace Back_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VwInfosController : ControllerBase
    {
        private readonly DbAPiContext _context;

        public VwInfosController(DbAPiContext context)
        {
            _context = context;
        }

        // GET: api/vwinfos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VwInfo>>> GetVwInfos()
        {
            return await _context.VwInfos.ToListAsync();
        }
        
    }
}
