﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Back_api.Models
{
    [Table("instrumento")]
    public partial class Instrumento
    {
        [Key]
        [Column("idInstrumento")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdInstrumento { get; set; }
        [Column("nombre")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Nombre { get; set; }
        [Column("idMarca")]
        public int? IdMarca { get; set; }
        [Column("fecha_creacion", TypeName = "datetime")]
        public DateTime? FechaCreacion { get; set; }
    }
}
