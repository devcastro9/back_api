﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Back_api.Models
{
    [Table("marca")]
    public partial class Marca
    {
        [Key]
        [Column("idMarca")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdMarca { get; set; }
        [Column("descripcion")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Descripcion { get; set; }
    }
}
