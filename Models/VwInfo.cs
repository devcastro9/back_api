﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Back_api.Models
{
    [Keyless]
    public partial class VwInfo
    {
        [Column("nombre")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Nombre { get; set; }
        [Column("descripcion")]
        [StringLength(50)]
        [Unicode(false)]
        public string? Descripcion { get; set; }
    }
}
